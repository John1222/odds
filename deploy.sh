#!/usr/bin/env bash

if [[ $EUID != 0 ]]; then
    sudo "$0" "$@"
    exit $?
fi

DOCKER_USER=docker
DOCKER_UID=1000

# создаем дефолтного юзера (или юзаем уже имеющегося)
if [[ -z $(getent passwd ${DOCKER_UID}) ]]
then
    if ! [[ -z $(getent passwd ${DOCKER_USER}) ]]
    then
        echo "We do not found user with UID=${DOCKER_UID}, but username ${DOCKER_USER} busy and we can't create this one. Please, create user with UID=${DOCKER_UID} or rename existing user with UID=${DOCKER_UID}"
        exit
    fi
    
    adduser --gecos "" ${DOCKER_USER}
    usermod -u ${DOCKER_UID} ${DOCKER_USER} && groupmod -g ${DOCKER_UID} ${DOCKER_USER}
    mkdir -p /var/www/html
    chown -R ${DOCKER_UID}:${DOCKER_UID} /var/www/html
    usermod -aG ${DOCKER_USER}
fi

apt-get update

# docker engine
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt install -y docker-ce=5:18.09* docker-ce-cli=5:18.09* containerd.io

# docker compose
curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# рабочая папка на хосте
WORKDIR="/var/www/html"
mkdir -p ${WORKDIR} && cd ${WORKDIR}

# репо
apt-get install git
git clone https://brocoder1@bitbucket.org/brocoder_team/odds.git .

# раздаем права
chown -R ${DOCKER_UID}:${DOCKER_UID} ${WORKDIR}
chmod -R u=rwx,g=r,o= ./app/bin
chmod -R u=rwx,g=r,o= ./bin

# тома
docker volume create app
docker volume create db

# подымаем контейнеры
docker-compose build
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d

# дампим проект внутрь тома
docker run --name in_volume_dumper -v app:/data -t -d ubuntu 
tar -C ./app -cv . | docker exec -i in_volume_dumper tar x -C /data
docker stop in_volume_dumper && docker rm in_volume_dumper

# composer deps
docker exec -it odds_phpfpm_1 bash -c "echo 'APP_ENV=dev' > ./.env && composer install --optimize-autoloader"

# накатываем бд
docker exec -it odds_phpfpm_1 ./bin/console doctrine:migrations:migrate -n

# стартуем тесты
docker exec -it odds_phpfpm_1 ./vendor/bin/codecept run

# небольшая фикстура
docker exec -it odds_phpfpm_1 ./bin/console doctrine:fixtures:load --group=AppFixtures -n