<?php
namespace App\Tests\Helper;

use Codeception\Exception\ModuleException;
use Codeception\Lib\Driver\PostgreSql;
use Codeception\Lib\ModuleContainer;
use Codeception\Module;

class Db extends Module
{
    private Module\Db $db;
    private int $opponentId = 1;
    private int $providerId = 1;
    private int $identityLinkId = 1;
    private int $languageId = 1;
    private int $sportId = 1;
    private int $championshipId = 1;
    
    public function _initialize()
    {
        $this->db = $this->getModule( 'Db' );
    }

    public function cleanup()
    {
        $this->db->driver->load( [
            '
DO
$$
    DECLARE
        statements CURSOR FOR
            SELECT
                tablename
            FROM
                pg_tables
            WHERE
                schemaname = \'public\' AND
                tablename != \'migration_versions\';
    BEGIN
        FOR stmt IN statements
            LOOP
                EXECUTE \'TRUNCATE TABLE \' || quote_ident(stmt.tablename) || \' RESTART IDENTITY CASCADE;\';
                EXECUTE \'ALTER SEQUENCE \' || quote_ident(stmt.tablename) || \'_id_seq RESTART WITH 1;\';
            END LOOP;
    END;
$$
'
        ] );

        $this->opponentId = 1;
        $this->providerId = 1;
        $this->identityLinkId = 1;
        $this->languageId = 1;
        $this->sportId = 1;
        $this->championshipId = 1;
    }

    public function createOpponent( string $name, int $sport, array $identityLinks = [] )
    {
        $id = $this->opponentId++;
        $this->db->haveInDatabase(
            'opponents',
            [
                'id' => $id,
                'sport_id' => $sport,
                'name' => $name
            ]
        );

        $identityLinks = ( count( $identityLinks ) == 0 ) ? [ $name ] : $identityLinks;
        foreach( $identityLinks as $name ) {
            $this->createIdentityLink( $name, null, $id );
        }
    }

    public function createProvider( string $name, array $identityLinks = [] )
    {
        $id = $this->providerId++;
        $this->db->haveInDatabase(
            'providers',
            [
                'id' => $id,
                'name' => $name
            ]
        );

        $identityLinks = ( count( $identityLinks ) == 0 ) ? [ $name ] : $identityLinks;
        foreach( $identityLinks as $name ) {
            $this->createIdentityLink( $name, null, null, $id );
        }
    }

    public function createSport( string $name, array $identityLinks = [] )
    {
        $id = $this->sportId++;
        $this->getModule( 'Db' )->haveInDatabase(
            'sports',
            [
                'id' => $id,
                'name' => $name
            ]
        );
        
        $identityLinks = ( count( $identityLinks ) == 0 ) ? [ $name ] : $identityLinks;
        foreach( $identityLinks as $name ) {
            $this->createIdentityLink( $name, null, null, null, null, $id );
        }
    }

    public function createLanguage( string $name, array $identityLinks = [] )
    {
        $id = $this->languageId++;
        $this->db->haveInDatabase(
            'languages',
            [
                'id' => $id,
                'name' => $name
            ]
        );
        
        $identityLinks = ( count( $identityLinks ) == 0 ) ? [ $name ] : $identityLinks;
        foreach( $identityLinks as $name ) {
            $this->createIdentityLink( $name, null, null, null, $id );
        }
    }

    public function createChampionship( string $name, int $sport, array $identityLinks = [] )
    {
        $id = $this->championshipId++;
        $this->db->haveInDatabase(
            'championships',
            [
                'id' => $id,
                'name' => $name,
                'sport_id' => $sport
            ]
        );

        $identityLinks = ( count( $identityLinks ) == 0 ) ? [ $name ] : $identityLinks;
        foreach( $identityLinks as $name ) {
            $this->createIdentityLink( $name, $id );
        }
    }

    private function createIdentityLink( string $name, ?int $championship = null, ?int $opponent = null,
                                        ?int $provider = null, ?int $language = null, ?int $sport = null )
    {
        $this->db->haveInDatabase(
            'identity_links',
            [
                'id' => $this->identityLinkId++,
                'name' => mb_convert_case( $name, MB_CASE_LOWER ),
                'championship_id' => $championship,
                'opponent_id' => $opponent,
                'provider_id' => $provider,
                'language_id' => $language,
                'sport_id' => $sport
            ]
        );
    }

    /**
     * @param int $expectedNumber
     * @param array $criteria
     */
    public function seeGameNumRecords( int $expectedNumber, array $criteria = [] )
    {
        $this->db->seeNumRecords( $expectedNumber, 'games', $criteria );
    }

    /**
     * @param int $expectedNumber
     * @param array $criteria
     */
    public function seeGameBufferedNumRecords( int $expectedNumber, array $criteria = [] )
    {
        $this->db->seeNumRecords( $expectedNumber, 'games_buffer', $criteria );
    }

    /**
     * @param int $expectedNumber
     * @param array $criteria
     */
    public function seeConfrontationNumRecords( int $expectedNumber, array $criteria = [] )
    {
        $this->db->seeNumRecords( $expectedNumber, 'confrontations', $criteria );
    }

    /**
     * @param int $langId
     * @param int $sportId
     * @param int $championshipId
     * @param int $confrontationId
     * @param string $time
     */
    public function seeGameInDatabase( $langId, $sportId, $championshipId, $confrontationId, $time )
    {
        $this->db->seeInDatabase(
            'games',
            [
                'language_id' => $langId,
                'sport_id' => $sportId,
                'championship_id' => $championshipId,
                'confrontation_id' => $confrontationId,
                'time' => $time
            ]
        );
    }

    /**
     * @param int $langId
     * @param int $sportId
     * @param int $championshipId
     * @param int $confrontationId
     * @param int $providerId
     * @param int $gameId
     * @param string $time
     */
    public function seeGameBufferedInDatabase(
        $langId, $sportId, $championshipId,
        $confrontationId, $providerId, $gameId, $time
    )
    {
        $this->db->seeInDatabase(
            'games_buffer',
            [
                'language_id' => $langId,
                'sport_id' => $sportId,
                'championship_id' => $championshipId,
                'confrontation_id' => $confrontationId,
                'provider_id' => $providerId,
                'game_id' => $gameId,
                'time' => $time
            ]
        );
    }

    /**
     * @param int $opponent1Id
     * @param int $opponent2Id
     */
    public function seeConfrontationInDatabase( int $opponent1Id, int $opponent2Id )
    {
        $this->db->seeInDatabase( 'confrontations', [
            'opponent1_id' => $opponent1Id, 'opponent2_id' => $opponent2Id
        ] );
    }
}