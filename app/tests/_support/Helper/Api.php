<?php
namespace App\Tests\Helper;

use Codeception\Exception\ModuleException;
use Codeception\Module;

class Api extends Module
{
    /**
     * @param string $lang
     * @param string $sport
     * @param string $championship
     * @param string $opponent1
     * @param string $opponent2
     * @param string $provider
     * @param string $time
     * @throws ModuleException
     */
    public function sendMERGE( $lang, string $sport, string $championship, string $opponent1, string $opponent2,
                               string $provider, string $time )
    {
        $this->getModule( 'REST' )->haveHttpHeader( 'Content-Type', 'application/json' );
        $this->getModule( 'REST' )->sendPOST(
            '/game_merge/',
            [
                'language' => $lang,
                'sport' => $sport,
                'championship' => $championship,
                'opponent1' => $opponent1,
                'opponent2' => $opponent2,
                'provider' => $provider,
                'time' => $time
            ]
        );
    }

    /**
     * @param array $games
     * @throws ModuleException
     */
    public function sendMERGEBatch( array $games )
    {
        $this->getModule( 'REST' )->haveHttpHeader( 'Content-Type', 'application/json' );
        $post = [];
        foreach( $games as $game ) {
            list( $lang, $sport, $championship, $opponent1, $opponent2, $provider, $time ) = $game;
            $post[] = [
                'language' => $lang,
                'sport' => $sport,
                'championship' => $championship,
                'opponent1' => $opponent1,
                'opponent2' => $opponent2,
                'provider' => $provider,
                'time' => $time
            ];
        }
        $this->getModule( 'REST' )->sendPOST(
            '/game_merge/',
            $post
        );
    }

    public function sendMERGEBatchMultiThreaded( array $games )
    {
        $mh = curl_multi_init();
        foreach( $games as $game ) {
            $post = '{ "language": "%s", "sport": "%s", "championship": "%s", "opponent1": "%s", "opponent2": "%s", "provider": "%s", "time": "%s" }';
            $post = sprintf(
                $post, $game[ 0 ], $game[ 1 ], $game[ 2 ],
                $game[ 3 ], $game[ 4 ], $game[ 5 ], $game[ 6 ]
            );
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $this->getModule( 'REST' )->_getConfig()[ 'url' ] . '/game_merge/' );
            curl_setopt( $ch, CURLOPT_HEADER, 0 );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen( $post )
            ] );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $post );
            curl_multi_add_handle( $mh, $ch );
        }

        $mrc = curl_multi_exec( $mh, $active );
        while( $active && $mrc == CURLM_OK ) {
            if( curl_multi_select( $mh ) == -1 ) {
                usleep( 100 );
            }
            do {
                $mrc = curl_multi_exec( $mh, $active );
            }
            while( $mrc == CURLM_CALL_MULTI_PERFORM );
        }
    }

    /**
     * @param array $json
     * @throws ModuleException
     */
    public function seeResponseEqualsToJson( array $json )
    {
        $response = $this->getModule( 'REST' )->response;
        $this->assertEquals( $json, json_decode( $response, true ) );
    }

    /**
     * @param int $gameId
     * @param string $sport
     * @param string $championship
     * @param string $opponent1
     * @param string $opponent2
     * @param array $gamesBuffered
     * @throws ModuleException
     */
    public function seeResponseEqualsTo( int $gameId, string $sport, string $championship, string $opponent1,
                                         string $opponent2, array $gamesBuffered )
    {
        $this->seeResponseEqualsToJson( [
            'id' => $gameId,
            'sport' => $sport,
            'championship' => $championship,
            'opponent1' => $opponent1,
            'opponent2' => $opponent2,
            'game_buffer' => $gamesBuffered,
        ] );
    }
}
