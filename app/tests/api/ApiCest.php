<?php
/** @noinspection PhpUnused */

namespace App\Tests;

use DateTime;

class ApiCest
{
    private ApiTester $I;

    public function _before( ApiTester $I )
    {
        $this->I = $I;
        $this->I->cleanup();

        $I->createLanguage( 'ru', [ 'ru', 'русский' ] );

        $I->createSport( 'football', [ 'football', 'futbol', 'футбол' ] );
        $I->createSport( 'hockey' );

        $I->createChampionship( 'Лига чемпионов УЕФА', 1, [ 'uefa', 'уефа' ] );
        $I->createChampionship( 'РФПЛ', 1, [ 'rfpl' ] );

        $this->I->createOpponent( 'Barcelona', 1, [ 'barcelona', 'barca' ] );
        $this->I->createOpponent( 'Real Madrid', 1, [ 'real madrid', 'real m' ] );
        $this->I->createOpponent( 'Athletico', 1 );

        for( $providerId = 1; $providerId <= 7; ++$providerId ) {
            $I->createProvider( "Provider {$providerId}", [ "prov{$providerId}.com" ] );
        }
    }

    /**
     * Добавление одной игры за раз
     */
    public function testMergingSingleGame()
    {
        $this->I->sendMERGE(
            'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-08-12 15:00:00'
        );

        $this->I->seeGameBufferedNumRecords( 1 );
        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 1, 1, '2019-08-12 15:00:00' );

        $this->I->seeGameNumRecords( 1 );
        $this->I->seeGameInDatabase( 1, 1, 1, 1, '2019-08-12 15:00:00' );
    }

    /**
     * Добавление нескольких игр пакетом
     */
    public function testMergingBatchOfGames()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-08-10 00:00:00', ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-08-11 00:00:01' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov3.com', '2019-08-12 00:00:02' ]
        ] );
        $this->I->seeGameBufferedNumRecords( 3 );
        $this->I->seeGameNumRecords( 3 );
    }

    /**
     * Если прилетела игра с самым ранним временем начала из всего присутствующего в буфере, то аттачим ее к этой игре,
     * если разница между ними не превышает суток
     */
    public function testMergingEarliestGame()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-11 20:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-10 20:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov3.com', '2019-06-09 19:59:59' ]
        ] );

        $this->I->seeGameBufferedNumRecords( 2, [ 'game_id' => 1 ] );
        $this->I->seeGameBufferedNumRecords( 1, [ 'game_id' => 2 ] );

        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 1, 1, '2019-06-11 20:00:00' );
        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 2, 1, '2019-06-10 20:00:00' );
        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 3, 2, '2019-06-09 19:59:59' );
    }

    /**
     * Полных дублей (по всем свойствам включая провайдера) в буфере быть не должно
     */
    public function testIgnoringDuplicatesInMerge()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 3:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 3:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-12 4:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-12 4:00:00' ],
        ] );
        $this->I->seeGameBufferedNumRecords( 2 );
    }

    /**
     * Окно времени начала игры, которое считается одной игрой, должно стартовать с самого раннего времени и
     * заканчиваться ровно через сутки. Т.е. из двух игр в game_buffer со временем начала 2:00 и 3:00 в качестве точки
     * отсчета окна будет выбрано 2:00. Т.о. любая игра с временем начала не далее чем 2:00 следующего дня будет
     * отнесена к этой.
     */
    public function testGameMergingWindow()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-11 00:00:01' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov3.com', '2019-06-11 10:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov4.com', '2019-06-11 20:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov5.com', '2019-06-12 1:00:00' ],
        ] );

        $this->I->seeGameBufferedNumRecords( 5 );
        $this->I->seeGameBufferedNumRecords( 3, [ 'game_id' => 2 ] );

        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 2, 2, '2019-06-11 00:00:01' );
        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 3, 2, '2019-06-11 10:00:00' );
        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 4, 2, '2019-06-11 20:00:00' );
    }

    /**
     * Массовый мердж в многопоточном режиме максимально приближает обстановку к боевой
     */
    public function testMergingInMultiThreadedMode()
    {
        $timeStart = '2019-06-10 00:00:00';
        $this->I->sendMERGEBatchMultiThreaded( $this->generateGamesForMerge( new DateTime( $timeStart ) ) );

        $this->I->seeGameNumRecords( 5 );
        $this->I->seeGameBufferedNumRecords( 20 );

        for( $gameId = 1; $gameId <= 5; ++$gameId ) {
            $this->I->seeGameBufferedNumRecords( 4, [ 'game_id' => $gameId ] );
            $this->I->seeGameInDatabase( 1, 1, 1, $gameId, $timeStart );
        }
    }

    private function generateGamesForMerge( DateTime $timeStart ): array
    {
        $opponents = [];
        for( $i = 0; $i < 10; ++$i ) {
            $opponentName = "team{$i}";
            $this->I->createOpponent( $opponentName, 1 );
            $opponents[] = $opponentName;
        }

        $games = [];
        $game = [ 'ru', 'football', 'UEFA', '%TEAM1%', '%TEAM2%', 'prov1.com', '%TIME%' ];
        while( 0 < count( $opponents ) ) {
            $time = clone $timeStart;
            $opponent1 = array_shift( $opponents );
            $opponent2 = array_shift( $opponents );

            for( $gamesBuffered = 0; $gamesBuffered < 4; ++$gamesBuffered ) {
                $game[ 3 ] = $opponent1;
                $game[ 4 ] = $opponent2;
                $game[ 6 ] = $time->format( 'Y-m-d H:i:s' );
                $games[] = $game;
                $time->modify( '+1 hours' );
            }
        }
        return $games;
    }

    /**
     * Между играми, отличающихся чемпионатом, спортом либо оппонентами мерджей быть не должно
     */
    public function testMergeIgnoringInSomeCases()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Athletico', 'Real Madrid', 'prov1.com', '2019-06-12 3:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-12 3:00:00' ],
            [ 'ru', 'hockey', 'UEFA', 'Barcelona', 'Real Madrid', 'prov3.com', '2019-06-12 3:00:00' ],
        ] );
        $this->I->seeGameBufferedNumRecords( 3 );
        $this->I->seeGameNumRecords( 3 );
    }

    /**
     * Если в game_buffer содержатся несолько смердженных записей с разным временем, то в game должно пойти
     * самое раннее
     */
    public function testChoosingEarliestBufferedTimeAsGameTime()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-11 10:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-11 16:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov4.com', '2019-06-11 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov3.com', '2019-06-11 22:00:00' ]
        ] );
        $this->I->seeGameNumRecords( 1 );
        $this->I->seeGameInDatabase( 1, 1, 1, 1, '2019-06-11 00:00:00' );
    }

    /**
     * Если в буфере содержится несколько записей от разных источников с разным временем начала игры, то в
     * качестве времени начала игры должно назначиться время, упоминаемое чаще всего
     */
    public function testChoosingMostPopularBufferedTimeAsGameTime()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov7.com', '2019-06-12 5:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 3:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov3.com', '2019-06-12 4:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-12 3:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov6.com', '2019-06-12 5:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov4.com', '2019-06-12 4:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov5.com', '2019-06-12 5:00:00' ]
        ] );

        // если контроллер перед выборкой данных сначала не загонит в буфер переданную игру, то определение времени
        // для записи в game будет производиться без учета последней переданной игры - в этом случае тест должен
        // упасть

        $this->I->seeGameBufferedNumRecords( 7 );
        $this->I->seeGameNumRecords( 1 );

        $this->I->seeGameInDatabase( 1, 1, 1, 1, '2019-06-12 5:00:00' );
    }

    /**
     * Проверяем обработку ошибок при попытке мерджа с некорректными данными
     */
    public function testHandlingInvalidInputOnGamesMerging()
    {
        $baseRequest = [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 00:00:00' ];
        $cases = [
            [ 'en', 'Do not found language "en"' ],
            [ 'literball', 'Do not found sport "literball"' ],
            [ 'CUEFA', 'Do not found championship "CUEFA"' ],
            [ 'Gazmyas', 'Do not found opponent "Gazmyas"' ],
            [ 'Gazmyas', 'Do not found opponent "Gazmyas"' ],
            [ 'gorod.mos.ru', 'Do not found provider "gorod.mos.ru"' ],
            [ 'holidays!', 'Time "holidays!" is invalid' ]
        ];
        foreach( $cases as $index => $case ) {
            $request = $baseRequest;
            $request[ $index ] = $case[ 0 ];
            $messageExpected = $case[ 1 ];

            $this->I->sendMERGEBatch( [ $request ] );
            $this->I->seeResponseIsJson();
            $this->I->seeResponseContainsJson( [ 'message' => $messageExpected ] );
        }
    }

    /**
     * Все даты игр должны приводиться к UTC
     */
    public function testMergedTimeShouldBeWithoutTimezone()
    {
        $this->I->sendMERGE(
            'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-08-12 18:00:00 Europe/Moscow'
        );
        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 1, 1, '2019-08-12 15:00:00' );
        $this->I->seeGameInDatabase( 1, 1, 1, 1, '2019-08-12 15:00:00' );
    }

    /**
     * Порядок указания оппонентов не должен иметь значения
     */
    public function testOpponentsConfrontation()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Real Madrid', 'Barcelona', 'prov2.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Athletico', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Athletico', 'Barcelona', 'prov2.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Athletico', 'Real Madrid', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Real Madrid', 'Athletico', 'prov2.com', '2019-08-10 00:00:00' ]
        ] );

        $this->I->seeGameBufferedNumRecords( 6 );
        $this->I->seeGameNumRecords( 3 );
        $this->I->seeConfrontationNumRecords( 3 );

        $this->I->seeConfrontationInDatabase( 1, 2 );
        $this->I->seeConfrontationInDatabase( 1, 3 );
        $this->I->seeConfrontationInDatabase( 3, 2 );
    }

    public function testGameGetterWithAndWithoutFilters()
    {
        $this->I->sendMERGEBatch( [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 01:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-12 02:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov2.com', '2019-06-12 03:00:00' ]
        ] );

        $this->I->seeGameBufferedNumRecords( 4 );
        $this->I->seeGameNumRecords( 1 );
        
        $cases = [
            // без фильтров
            [
                'id=1',
                [
                    [ 'time' => '2019-06-12 00:00:00', 'provider' => 'Provider 1' ],
                    [ 'time' => '2019-06-12 01:00:00', 'provider' => 'Provider 1' ],
                    [ 'time' => '2019-06-12 02:00:00', 'provider' => 'Provider 2' ],
                    [ 'time' => '2019-06-12 03:00:00', 'provider' => 'Provider 2' ]
                ],
            ],
            // с фильтром по прову
            [
                'id=1&provider=prov2.com',
                [
                    [ 'time' => '2019-06-12 02:00:00', 'provider' => 'Provider 2' ],
                    [ 'time' => '2019-06-12 03:00:00', 'provider' => 'Provider 2' ]
                ]
            ],
            // с фильтром только по time_from
            [
                'id=1&time_from=2019-06-12 02:00:00',
                [
                    [ 'time' => '2019-06-12 02:00:00', 'provider' => 'Provider 2' ],
                    [ 'time' => '2019-06-12 03:00:00', 'provider' => 'Provider 2' ]
                ]
            ],
            // с фильтром только по time_to
            [
                'id=1&time_to=2019-06-12 01:00:00',
                [
                    [ 'time' => '2019-06-12 00:00:00', 'provider' => 'Provider 1' ],
                    [ 'time' => '2019-06-12 01:00:00', 'provider' => 'Provider 1' ]
                ]
            ],
            // с фильтром только по диапазону дат
            [
                'id=1&time_from=2019-06-12 01:00:00&time_to=2019-06-12 02:00:00',
                [
                    [ 'time' => '2019-06-12 01:00:00', 'provider' => 'Provider 1' ],
                    [ 'time' => '2019-06-12 02:00:00', 'provider' => 'Provider 2' ]
                ]
            ],
            // фильтр по прову и дате
            [
                'id=1&provider=prov2.com&time_from=2019-06-12 02:00:00',
                [
                    [ 'time' => '2019-06-12 02:00:00', 'provider' => 'Provider 2' ],
                    [ 'time' => '2019-06-12 03:00:00', 'provider' => 'Provider 2' ]
                ]
            ]
        ];

        foreach( $cases as $case ) {
            $this->I->sendGET( "/game_get/?{$case[ 0 ]}" );
            $this->I->seeResponseIsJson();
            $this->I->seeResponseEqualsTo(
                1, 'football', 'Лига чемпионов УЕФА', 'Barcelona', 'Real Madrid', $case[ 1 ]
            );
        }
    }

    /**
     * Проверяем реакцию геттера на выборку по несуществующей игре, кривой дате либо несуществующему провайдеру
     */
    public function testHandlingInvalidInputOnGamesGetting()
    {
        $this->I->sendMERGE(
            'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-06-12 00:00:00'
        );
        $cases = [
            [ 'id=1000', 'Do not found game "1000"' ],
            [ 'id=1&time_from=20192233', 'Time "20192233" is invalid' ],
            [ 'id=1&time_to=20192233', 'Time "20192233" is invalid' ],
            [ 'id=1&provider=prov100500.com', 'Do not found provider "prov100500.com"' ],
        ];
        foreach( $cases as $case ) {
            $this->I->sendGET( "/game_get/?{$case[ 0 ]}" );
            $this->I->seeResponseIsJson();
            $this->I->seeResponseContainsJson( [ 'message' => $case[ 1 ] ] );
        }
    }

    /**
     * Проверка механизма IdentityLink - поиска существующих сущностей в БД путем сопоставления названий сущностей,
     * пришедших в запросе с их ожидаемыми названиями, перечисленными в таблице identity_links. Регистр не должен иметь
     * значения
     */
    public function testEntityIdentificationMechanism()
    {
        $input = [
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'русский', 'football', 'UEFA', 'Barcelona', 'Real Madrid', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barcelona', 'Real M', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'football', 'UEFA', 'Barca', 'Real M', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'futbol', 'UEFA', 'Barca', 'Real M', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'футбол', 'UEFA', 'Barca', 'Real M', 'prov1.com', '2019-08-10 00:00:00' ],
            [ 'ru', 'футбол', 'УЕФА', 'Barca', 'Real M', 'prov1.com', '2019-08-10 00:00:00' ]
        ];
        $inputWithCaseRandomizedValues = array_map( function( $array ) {
            return array_map( function( $value ) {
                $valueNew = '';
                foreach( preg_split( '//u', $value, null, PREG_SPLIT_NO_EMPTY ) as $char ) {
                    $valueNew .=
                        ( mt_rand( 0, 1 ) === 0 ) ?
                            mb_convert_case( $char, MB_CASE_UPPER ) :
                            mb_convert_case( $char, MB_CASE_LOWER );
                }
                return $valueNew;
            }, $array );
        }, $input );

        $this->I->sendMERGEBatch( $inputWithCaseRandomizedValues );

        $this->I->seeGameBufferedNumRecords( 1 );
        $this->I->seeConfrontationNumRecords( 1 );

        $this->I->seeGameBufferedInDatabase( 1, 1, 1, 1, 1, 1, '2019-08-10 00:00:00' );
        $this->I->seeGameInDatabase( 1, 1, 1, 1, '2019-08-10 00:00:00' );
    }
}