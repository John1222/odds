<?php

namespace App\DataFixtures;

use App\Entity\Championship;
use App\Entity\IdentityLink;
use App\Entity\Language;
use App\Entity\Opponent;
use App\Entity\Provider;
use App\Entity\Sport;
use App\Service\GameMerger;
use App\Service\GameMergeRequest;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load( ObjectManager $manager )
    {
        /*********************
         ** Languages
         *********************/
        
        $langs = [
            'ru' => [ 'ru', 'rus', 'русский' ]
        ];
        foreach( $langs as $name => $linkNames ) {
            $lang = new Language;
            $lang->setName( $name );
            $manager->persist( $lang );

            foreach( $linkNames as $linkName ) {
                $link = new IdentityLink;
                $link->setName( $linkName );
                $link->setLanguage( $lang );
                $manager->persist( $link );
            }
        }

        /*********************
         ** Sports
         *********************/
        
        $sports = [
            'football' => [ 'football', 'futbal', 'футбол' ]
        ];
        $sportLast = null;
        foreach( $sports as $name => $linkNames ) {
            $sportLast = new Sport;
            $sportLast->setName( $name );
            $manager->persist( $sportLast );

            foreach( $linkNames as $linkName ) {
                $link = new IdentityLink;
                $link->setName( $linkName );
                $link->setSport( $sportLast );
                $manager->persist( $link );
            }
        }
        
        /*********************
         ** Championships
         *********************/
        
        $champs = [
            'Лига чемпионов УЕФА' => [ 'uefa', 'уефа' ],
            'Российская Футбольная Премьер-Лига' => [ 'rfpl', 'рфпл' ],
        ];
        foreach( $champs as $name => $linkNames ) {
            $champ = new Championship;
            $champ->setName( $name );
            $champ->setSport( $sportLast );
            $manager->persist( $champ );

            foreach( $linkNames as $linkName ) {
                $link = new IdentityLink;
                $link->setName( $linkName );
                $link->setChampionship( $champ );
                $manager->persist( $link );
            }
        }

        /*********************
         ** Opponents
         *********************/
        $opponents = [
            'Barcelona'   => [ 'barcelona', 'barca' ],
            'Real Madrid' => [ 'real madrid', 'real m' ]
        ];
        foreach( $opponents as $name => $linkNames ) {
            $opponent = new Opponent;
            $opponent->setName( $name );
            $opponent->setSport( $sportLast );
            $manager->persist( $opponent );

            foreach( $linkNames as $linkName ) {
                $link = new IdentityLink;
                $link->setName( $linkName );
                $link->setOpponent( $opponent );
                $manager->persist( $link );
            }
        }

        /*********************
         ** Providers
         *********************/
        
        $providers = [
            'Provider 1' => 'prov1.com',
            'Provider 2' => 'prov2.com',
            'Provider 3' => 'prov3.com'
        ];
        foreach( $providers as $name => $linkName ) {
            $provider = new Provider;
            $provider->setName( $name );
            
            $link = new IdentityLink;
            $link->setName( $linkName );
            $link->setProvider( $provider );

            $manager->persist( $provider );
            $manager->persist( $link );
        }
        
        /*********************
         ** Flush
         *********************/
        
        $manager->flush();

        /*********************
         ** Games
         *********************/
        
        $game = new GameMergeRequest( $manager );
        $game->setSport( 'футбол' );
        $game->setChampionship( 'uefa' );
        $game->setProvider( 'prov1.com' );
        $game->setLanguage( 'ru' );
        $game->setOpponent1( 'real m' );
        $game->setOpponent2( 'barcelona' );
        $game->setTime( '2019-06-11 20:00:00' );
        
        $merger = new GameMerger( $manager );
        $merger->merge( [ $game ] );
    }
}
