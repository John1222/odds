<?php
/** @noinspection PhpUnused */
/** @noinspection PhpMissingFieldTypeInspection */

namespace App\DataFixtures;

use App\Entity\Championship;
use App\Entity\Confrontation;
use App\Entity\IdentityLink;
use App\Entity\Language;
use App\Entity\Opponent;
use App\Entity\Provider;
use App\Entity\Sport;
use Ayesh\PHP_Timer\Timer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Если задумаем импортировать неебические объемы (примерно от 30 млн), то если не хотим ждать до второго
 * пришествия, внимаем:
 *   full_page_writes=off, wal_level=minimal, archive_mode=off, max_wal_senders=0, synchronous_commit=off
 *   сносим индексы, после - добавляем заново
 *   констрейнты фикстура на время оффнет сама
 *   подымаем значения maintenance_work_mem и work_mem
 * 
 * 120 лямов у меня заезжало примерно за полчаса, но я собственные же рекомендации юзал лишь отчасти.
 */
class BigDataFixtures extends Fixture implements FixtureInterface, ContainerAwareInterface
{
    /**
     * Сколько всего записей в game будет создано
     * @var int
     */
    private const GAMES_TO_CREATE = 1000 * 1000 * 10;
    /**
     * Сколько будет создано записей в games_buffer на каждую запись в game
     * @var int
     */
    private const GAMES_BUFFERED_PER_GAME_TO_CREATE = 3;
    /**
     * Сколько будет создано записей в confrontations (количество оппонентов будет ровно вдвое больше)
     * @var int
     */
    private const CONFRONTATIONS_TO_CREATE = 5000;
    /**
     * По сколько записей будет содержать каждая партия записей (на 10млн у меня падало; на скорость почти не влияет).
     * Рекомендуемое значение: не более 5млн
     * @var int
     */
    private const CHUNK_BY = 1000 * 1000 * 1;
    /**
     * Полный путь до папки, куда будут складываться временные csv с записями. (Папка должна быть доступна
     * одновременно с контейнеров db и phpfpm.)
     * @var string
     */
    private const CSV_OUTPUT_FOLDER = '/var/www/html/var/csv';

    private ContainerInterface $container;
    private EntityManager $em;
    private Connection $conn;

    /**
     * @var string[]
     */
    private array $gamesBufferedCsv = [];
    /**
     * @var string[]
     */
    private array $gamesCsv = [];
    /**
     * @var string[]
     */
    private array $insertQueries = [];
    private int $gameId = 1;
    private int $gameBufferedId = 1;
    private int $lastFlushedGameBufferedId = 0;
    private int $lastFlushedGameId = 0;

    public function setContainer( ContainerInterface $container = null )
    {
        $this->container = $container;
        $this->em = $this->container->get( 'doctrine' )->getManager();
        $this->conn = $this->container->get( 'doctrine' )->getConnection();
    }

    public function load( ObjectManager $manager )
    {
        $this->cleanCsvFolder();

        $providers = $this->createProviders( self::GAMES_BUFFERED_PER_GAME_TO_CREATE );
        $lang = $this->createLang();
        $sport = $this->createSport();
        $championship = $this->createChampionship( $sport );
        $confrontations = $this->createConfrontations( self::CONFRONTATIONS_TO_CREATE, $sport );

        $this->buildDataInCsvFormat( $confrontations, $lang, $sport, $championship, $providers );
        $this->importCsvInDb();
        $this->showPerformanceReport();
        
        $this->cleanCsvFolder();
    }

    /**
     * @return int[]
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createProviders( int $amount )
    {
        $providers = [];
        for( $i = 1; $i <= $amount; ++$i ) {
            $name = "prov{$i}.com";
            
            $provider = new Provider;
            $provider->setName( $name );
            $this->em->persist( $provider );
            $providers[] = $provider->getId();

            $link = new IdentityLink;
            $link->setName( $name );
            $link->setProvider( $provider );
            $this->em->persist( $link );
        }
        $this->em->flush();
        return $providers;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createLang(): Language
    {
        $lang = new Language;
        $lang->setName( 'russia' );
        $this->em->persist( $lang );

        $link = new IdentityLink;
        $link->setName( 'ru' );
        $link->setLanguage( $lang );
        $this->em->persist( $link );

        $link = new IdentityLink;
        $link->setName( 'русский$' );
        $link->setLanguage( $lang );
        $this->em->persist( $link );
        
        $this->em->flush();
        return $lang;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createSport(): Sport
    {
        $sportLast = new Sport;
        $sportLast->setName( 'football' );
        $this->em->persist( $sportLast );
        
        foreach( [ 'football', 'futbal', 'футбол' ] as $linkName ) {
            $link = new IdentityLink;
            $link->setName( $linkName );
            $link->setSport( $sportLast );
            $this->em->persist( $link );
        }
        
        $this->em->flush();
        return $sportLast;
    }

    /**
     * @return Championship
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createChampionship( Sport $sport ): Championship
    {
        $championship = new Championship;
        $championship->setName( 'Лига чемпионов УЕФА' );
        $championship->setSport( $sport );

        $link = new IdentityLink;
        $link->setName( 'uefa' );
        $link->setChampionship( $championship );
        $this->em->persist( $link );

        $link = new IdentityLink;
        $link->setName( 'уефа' );
        $link->setChampionship( $championship );
        $this->em->persist( $link );

        $this->em->persist( $link );
        $this->em->persist( $championship );
        $this->em->flush();
        return $championship;
    }

    /**
     * @return int[]
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function createConfrontations( int $confrontationsToCreate, Sport $sport )
    {
        $confrontations = [];
        $opponentId = 0;
        for( $i = 0; $i < $confrontationsToCreate; ++$i ) {
            $opponent1Name = 'team' . ( ++$opponentId );
            $opponent2Name = 'team' . ( ++$opponentId );

            $opponent1 = new Opponent;
            $opponent1->setName( $opponent1Name );
            $opponent1->setSport( $sport );
            $this->em->persist( $opponent1 );

            $opponent2 = new Opponent;
            $opponent2->setName( $opponent2Name );
            $opponent2->setSport( $sport );
            $this->em->persist( $opponent2 );

            $link = new IdentityLink;
            $link->setName( $opponent1Name );
            $link->setOpponent( $opponent1 );
            $this->em->persist( $link );

            $link = new IdentityLink;
            $link->setName( $opponent2Name );
            $link->setOpponent( $opponent2 );
            $this->em->persist( $link );

            $confrontation = new Confrontation;
            $confrontation->setOpponent1( $opponent1 );
            $confrontation->setOpponent2( $opponent2 );
            $this->em->persist( $confrontation );
            
            $confrontations[] = $confrontation->getId();
            
            if( $i % 1000 === 0 ) {
                $this->em->flush();
            }
        }
        $this->em->flush();

        return $confrontations;
    }

    /**
     * @param int[] $confrontations
     * @param int[] $providers
     */
    private function buildDataInCsvFormat( $confrontations, Language $lang, Sport $sport,
                                           Championship $championship, $providers )
    {
        Timer::start( 'csv_builder' );
        $gamesBufferedRequired = count( $providers );
        for( $gameIdx = 1; $gameIdx <= self::GAMES_TO_CREATE; ++$gameIdx ) {
            $confrontation = $confrontations[ array_rand( $confrontations ) ];
            $date = date( 'Y-m-d H:i:s', mt_rand( 1, 2147385600 ) );

            $this->generateGameCsv(
                $confrontation, $lang->getId(), $sport->getId(),
                $championship->getId(), $date
            );

            for( $gameBufferedIdx = 1; $gameBufferedIdx <= $gamesBufferedRequired; ++$gameBufferedIdx ) {
                $this->generateGameBufferedCsv(
                    $providers[ $gameBufferedIdx - 1 ], $confrontation,
                    $lang->getId(), $sport->getId(), $championship->getId(),
                    $this->gameId, $date
                );
                $this->flushCsv();
            }
            
            $this->flushCsv();
        }
        Timer::stop( 'csv_builder' );
    }

    private function generateGameCsv( int $confrontation, int $lang, int $sport, int $championship, string $date )
    {
        $this->gamesCsv[] = sprintf(
            "%s, %s, %s, %s, %s, %s",
            $this->gameId, $confrontation, $lang, $sport, $championship, $date
        );
        $this->gameId++;
    }

    private function generateGameBufferedCsv( int $provider, int $confrontation, int $lang, int $sport,
                                              int $championship, int $game, string $date )
    {
        $this->gamesBufferedCsv[] = sprintf(
            '%s, %s, %s, %s, %s, %s, %s, %s',
            $this->gameBufferedId, $provider, $confrontation, $lang,
            $sport, $championship, $game, $date
        );
        $this->gameBufferedId++;
    }

    private function flushCsv()
    {
        $chunkBy = self::CHUNK_BY;
        $chunkBy = ( self::GAMES_TO_CREATE < $chunkBy ) ? self::GAMES_TO_CREATE : $chunkBy;
        if( ( $this->gameBufferedId % $chunkBy === 0 ) &&
            ( $this->lastFlushedGameBufferedId < $this->gameBufferedId ) ) {
            $dumpName = self::CSV_OUTPUT_FOLDER . "/games_buffered#{$this->gameBufferedId}.csv";
            $this->insertQueries[] = "COPY games_buffer FROM '{$dumpName}' DELIMITER ',';";
            file_put_contents( $dumpName, implode( "\n", $this->gamesBufferedCsv ) );
            $this->gamesBufferedCsv = [];
            $this->lastFlushedGameBufferedId = $this->gameBufferedId;
        }
        if( ( $this->gameId % $chunkBy === 0 ) && ( $this->lastFlushedGameId < $this->gameId ) ) {
            $dumpName = self::CSV_OUTPUT_FOLDER . "/games#{$this->gameId}.csv";
            $this->insertQueries[] = "COPY games FROM '{$dumpName}' DELIMITER ',';";
            file_put_contents( $dumpName, implode( "\n", $this->gamesCsv ) );
            $this->gamesCsv = [];
            $this->lastFlushedGameId = $this->gameId;

            $this->log( sprintf(
                "Generation progress: %s%%",
                round( $this->gameId / self::GAMES_TO_CREATE * 100 )
            ) );
        }
    }

    private function importCsvInDb()
    {
        $this->conn->exec( 'ALTER TABLE games DISABLE TRIGGER ALL;' );
        $this->conn->exec( 'ALTER TABLE games_buffer DISABLE TRIGGER ALL;' );
        
        Timer::start( 'insertion' );
        
        $queriesAll = count( $this->insertQueries );
        foreach( $this->insertQueries as $queriesExecuted => $query ) {
            $this->conn->exec( $query );
            $this->log( sprintf(
                "%s :: Insertion progress: %s%%",
                date( 'Y-m-d H:i:s' ),
                round( ( $queriesExecuted + 1 ) / $queriesAll * 100 )
            ) );
        }
        
        Timer::stop( 'insertion' );
        
        $this->conn->exec( "SELECT setval('games_id_seq', COALESCE((SELECT MAX(id) + 1 FROM games), 1), false);" );
        $this->conn->exec( "SELECT setval('games_buffer_id_seq', COALESCE((SELECT MAX(id) + 1 FROM games), 1), false);" );

        $this->conn->exec( 'ALTER TABLE games ENABLE TRIGGER ALL;' );
        $this->conn->exec( 'ALTER TABLE games_buffer ENABLE TRIGGER ALL;' );
    }

    private function showPerformanceReport()
    {
        $insertedRecordsAll = self::GAMES_TO_CREATE * self::GAMES_BUFFERED_PER_GAME_TO_CREATE;
        $this->log();
        $this->log( sprintf(
            'CSV building time spent: %s (%sk/sec)',
            gmdate( 'H:i:s', Timer::read( 'csv_builder', Timer::FORMAT_SECONDS ) ),
            round( $insertedRecordsAll / Timer::read( 'csv_builder', Timer::FORMAT_SECONDS ) )
        ) );
        $this->log( sprintf(
            'Insertion time spent: %s (%sk/sec)',
            gmdate( 'H:i:s', Timer::read( 'insertion', Timer::FORMAT_SECONDS ) ),
            round( $insertedRecordsAll / Timer::read( 'insertion', Timer::FORMAT_SECONDS ) )
        ) );
    }

    private function cleanCsvFolder()
    {
        if( file_exists( self::CSV_OUTPUT_FOLDER ) ) {
            foreach( glob( self::CSV_OUTPUT_FOLDER . '/*' ) as $file ) {
                unlink( $file );
            }
        }
        else {
            mkdir( self::CSV_OUTPUT_FOLDER, 0777, true );
        }
    }

    private function log( string $text = '' )
    {
        echo "{$text}\n";
    }
}
