<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200128165459 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE "championships_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "confrontations_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "games_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "games_buffer_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "identity_links_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "languages_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "opponents_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "providers_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "sports_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "championships" (id INT NOT NULL, sport_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B682EA93AC78BCF8 ON "championships" (sport_id)');
        $this->addSql('CREATE TABLE "confrontations" (id INT NOT NULL, opponent1_id INT DEFAULT NULL, opponent2_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_258BAA995976D600 ON "confrontations" (opponent1_id)');
        $this->addSql('CREATE INDEX IDX_258BAA994BC379EE ON "confrontations" (opponent2_id)');
        $this->addSql('CREATE TABLE "games" (id INT NOT NULL, confrontation_id INT DEFAULT NULL, language_id INT DEFAULT NULL, sport_id INT DEFAULT NULL, championship_id INT DEFAULT NULL, time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FF232B3118EE86FA ON "games" (confrontation_id)');
        $this->addSql('CREATE INDEX IDX_FF232B3182F1BAF4 ON "games" (language_id)');
        $this->addSql('CREATE INDEX IDX_FF232B31AC78BCF8 ON "games" (sport_id)');
        $this->addSql('CREATE INDEX IDX_FF232B3194DDBCE9 ON "games" (championship_id)');
        $this->addSql('CREATE INDEX idx_games_time ON "games" (time)');
        $this->addSql('CREATE TABLE "games_buffer" (id INT NOT NULL, provider_id INT DEFAULT NULL, confrontation_id INT DEFAULT NULL, language_id INT DEFAULT NULL, sport_id INT DEFAULT NULL, championship_id INT DEFAULT NULL, game_id INT NOT NULL, time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F3721F47A53A8AA ON "games_buffer" (provider_id)');
        $this->addSql('CREATE INDEX IDX_F3721F4718EE86FA ON "games_buffer" (confrontation_id)');
        $this->addSql('CREATE INDEX IDX_F3721F4782F1BAF4 ON "games_buffer" (language_id)');
        $this->addSql('CREATE INDEX IDX_F3721F47AC78BCF8 ON "games_buffer" (sport_id)');
        $this->addSql('CREATE INDEX IDX_F3721F4794DDBCE9 ON "games_buffer" (championship_id)');
        $this->addSql('CREATE INDEX IDX_F3721F47E48FD905 ON "games_buffer" (game_id)');
        $this->addSql('CREATE INDEX idx_games_buffer_time ON "games_buffer" (time)');
        $this->addSql('CREATE TABLE "identity_links" (id INT NOT NULL, opponent_id INT DEFAULT NULL, language_id INT DEFAULT NULL, championship_id INT DEFAULT NULL, sport_id INT DEFAULT NULL, provider_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3BD9EBE87F656CDC ON "identity_links" (opponent_id)');
        $this->addSql('CREATE INDEX IDX_3BD9EBE882F1BAF4 ON "identity_links" (language_id)');
        $this->addSql('CREATE INDEX IDX_3BD9EBE894DDBCE9 ON "identity_links" (championship_id)');
        $this->addSql('CREATE INDEX IDX_3BD9EBE8AC78BCF8 ON "identity_links" (sport_id)');
        $this->addSql('CREATE INDEX IDX_3BD9EBE8A53A8AA ON "identity_links" (provider_id)');
        $this->addSql('CREATE UNIQUE INDEX idx_unq_name_and_opponent ON "identity_links" (name, opponent_id)');
        $this->addSql('CREATE UNIQUE INDEX idx_unq_name_and_lang ON "identity_links" (name, language_id)');
        $this->addSql('CREATE UNIQUE INDEX idx_unq_name_and_champ ON "identity_links" (name, championship_id)');
        $this->addSql('CREATE UNIQUE INDEX idx_unq_name_and_sport ON "identity_links" (name, sport_id)');
        $this->addSql('CREATE TABLE "languages" (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "opponents" (id INT NOT NULL, sport_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_36A512ACAC78BCF8 ON "opponents" (sport_id)');
        $this->addSql('CREATE TABLE "providers" (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "sports" (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE "championships" ADD CONSTRAINT FK_B682EA93AC78BCF8 FOREIGN KEY (sport_id) REFERENCES "sports" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "confrontations" ADD CONSTRAINT FK_258BAA995976D600 FOREIGN KEY (opponent1_id) REFERENCES "opponents" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "confrontations" ADD CONSTRAINT FK_258BAA994BC379EE FOREIGN KEY (opponent2_id) REFERENCES "opponents" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games" ADD CONSTRAINT FK_FF232B3118EE86FA FOREIGN KEY (confrontation_id) REFERENCES "confrontations" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games" ADD CONSTRAINT FK_FF232B3182F1BAF4 FOREIGN KEY (language_id) REFERENCES "languages" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games" ADD CONSTRAINT FK_FF232B31AC78BCF8 FOREIGN KEY (sport_id) REFERENCES "sports" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games" ADD CONSTRAINT FK_FF232B3194DDBCE9 FOREIGN KEY (championship_id) REFERENCES "championships" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games_buffer" ADD CONSTRAINT FK_F3721F47A53A8AA FOREIGN KEY (provider_id) REFERENCES "providers" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games_buffer" ADD CONSTRAINT FK_F3721F4718EE86FA FOREIGN KEY (confrontation_id) REFERENCES "confrontations" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games_buffer" ADD CONSTRAINT FK_F3721F4782F1BAF4 FOREIGN KEY (language_id) REFERENCES "languages" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games_buffer" ADD CONSTRAINT FK_F3721F47AC78BCF8 FOREIGN KEY (sport_id) REFERENCES "sports" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games_buffer" ADD CONSTRAINT FK_F3721F4794DDBCE9 FOREIGN KEY (championship_id) REFERENCES "championships" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "games_buffer" ADD CONSTRAINT FK_F3721F47E48FD905 FOREIGN KEY (game_id) REFERENCES "games" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "identity_links" ADD CONSTRAINT FK_3BD9EBE87F656CDC FOREIGN KEY (opponent_id) REFERENCES "opponents" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "identity_links" ADD CONSTRAINT FK_3BD9EBE882F1BAF4 FOREIGN KEY (language_id) REFERENCES "languages" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "identity_links" ADD CONSTRAINT FK_3BD9EBE894DDBCE9 FOREIGN KEY (championship_id) REFERENCES "championships" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "identity_links" ADD CONSTRAINT FK_3BD9EBE8AC78BCF8 FOREIGN KEY (sport_id) REFERENCES "sports" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "identity_links" ADD CONSTRAINT FK_3BD9EBE8A53A8AA FOREIGN KEY (provider_id) REFERENCES "providers" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "opponents" ADD CONSTRAINT FK_36A512ACAC78BCF8 FOREIGN KEY (sport_id) REFERENCES "sports" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "games" DROP CONSTRAINT FK_FF232B3194DDBCE9');
        $this->addSql('ALTER TABLE "games_buffer" DROP CONSTRAINT FK_F3721F4794DDBCE9');
        $this->addSql('ALTER TABLE "identity_links" DROP CONSTRAINT FK_3BD9EBE894DDBCE9');
        $this->addSql('ALTER TABLE "games" DROP CONSTRAINT FK_FF232B3118EE86FA');
        $this->addSql('ALTER TABLE "games_buffer" DROP CONSTRAINT FK_F3721F4718EE86FA');
        $this->addSql('ALTER TABLE "games_buffer" DROP CONSTRAINT FK_F3721F47E48FD905');
        $this->addSql('ALTER TABLE "games" DROP CONSTRAINT FK_FF232B3182F1BAF4');
        $this->addSql('ALTER TABLE "games_buffer" DROP CONSTRAINT FK_F3721F4782F1BAF4');
        $this->addSql('ALTER TABLE "identity_links" DROP CONSTRAINT FK_3BD9EBE882F1BAF4');
        $this->addSql('ALTER TABLE "confrontations" DROP CONSTRAINT FK_258BAA995976D600');
        $this->addSql('ALTER TABLE "confrontations" DROP CONSTRAINT FK_258BAA994BC379EE');
        $this->addSql('ALTER TABLE "identity_links" DROP CONSTRAINT FK_3BD9EBE87F656CDC');
        $this->addSql('ALTER TABLE "games_buffer" DROP CONSTRAINT FK_F3721F47A53A8AA');
        $this->addSql('ALTER TABLE "identity_links" DROP CONSTRAINT FK_3BD9EBE8A53A8AA');
        $this->addSql('ALTER TABLE "championships" DROP CONSTRAINT FK_B682EA93AC78BCF8');
        $this->addSql('ALTER TABLE "games" DROP CONSTRAINT FK_FF232B31AC78BCF8');
        $this->addSql('ALTER TABLE "games_buffer" DROP CONSTRAINT FK_F3721F47AC78BCF8');
        $this->addSql('ALTER TABLE "identity_links" DROP CONSTRAINT FK_3BD9EBE8AC78BCF8');
        $this->addSql('ALTER TABLE "opponents" DROP CONSTRAINT FK_36A512ACAC78BCF8');
        $this->addSql('DROP SEQUENCE "championships_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "confrontations_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "games_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "games_buffer_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "identity_links_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "languages_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "opponents_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "providers_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "sports_id_seq" CASCADE');
        $this->addSql('DROP TABLE "championships"');
        $this->addSql('DROP TABLE "confrontations"');
        $this->addSql('DROP TABLE "games"');
        $this->addSql('DROP TABLE "games_buffer"');
        $this->addSql('DROP TABLE "identity_links"');
        $this->addSql('DROP TABLE "languages"');
        $this->addSql('DROP TABLE "opponents"');
        $this->addSql('DROP TABLE "providers"');
        $this->addSql('DROP TABLE "sports"');
    }
}
