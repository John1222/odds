<?php
/** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сущность заключает в себе противоборство двух команд. Может возникнуть вопрос: зачем нужна лишняя сущность,
 * когда можно напрямую хранить оппонентов в играх?
 * 
 * Узкое место здесь в том, что оппоненты могут быть в разной последовательности, но это по-прежнему одно и то же
 * противоборство. Потому изначально оппоненты писались сразу в игры и все выборки происходили через OR:
 * (opponent1 = 1 AND opponent2 = 2) OR (opponent2 = 1 AND opponent1 = 2)
 * Однако на практике это превращается в геморрой. Теперь на любую выборку с участием оппонентов мы
 * должны везде присовокуплять этот чертов OR, и не дай бог чо перепутать! А чтобы ничо не сломалось мы каждый
 * такой запрос вынуждены сопровождать тестами на перестановку оппонентов. Выходит, мы только что добавили целый слой
 * сложности! Это ладно в моем нубопроекте три таких запроса, а теперь вообразим себе прод с десятками и сотнями таких
 * вот демонических выборок!
 * 
 * Confrontation же выделен в отдельную структуру и стремный OR теперь всего один - в репозитории. Итоговые
 * плюшки:
 * 1) опасная и хрупкая логика сконцентрирована в одном месте
 * 2) простота тестирования: логику выборки протиборства легко затестить в репозитории
 * 3) оптимизация БД: вместо двух полей у нас только одно
 * 
 * @ORM\Entity(repositoryClass="App\Repository\ConfrontationRepository")
 * @ORM\Table(name="`confrontations`")
 */
class Confrontation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Opponent")
     */
    protected Opponent $opponent1;

    /**
     * @ORM\ManyToOne(targetEntity="Opponent")
     */
    protected Opponent $opponent2;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOpponent1(): Opponent
    {
        return $this->opponent1;
    }

    public function setOpponent1( Opponent $opponent1 )
    {
        $this->opponent1 = $opponent1;
    }

    public function getOpponent2(): Opponent
    {
        return $this->opponent2;
    }

    public function setOpponent2( Opponent $opponent2 )
    {
        $this->opponent2 = $opponent2;
    }
}