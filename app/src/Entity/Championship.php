<?php
/** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`championships`")
 **/
class Championship
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected int $id;

    /**
     * @ORM\Column(type="string")
     */
    protected string $name;

    /**
     * @ORM\ManyToOne(targetEntity="Sport")
     */
    protected Sport $sport;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName( string $name )
    {
        $this->name = $name;
    }

    public function getSport(): Sport
    {
        return $this->sport;
    }

    public function setSport( Sport $sport )
    {
        $this->sport = $sport;
    }
}