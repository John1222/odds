<?php
/** @noinspection PhpUnused */

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="`games`", indexes={
 *   @ORM\Index(name="idx_games_time", columns={"time"})
 * })
 **/
// TODO добавить бы флаг closed/active. И в буфер тоже. Закрытые игры сразу отваливаются от запросов
class Game
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected int $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Confrontation")
     */
    protected Confrontation $confrontation;
    
    /**
     * @ORM\ManyToOne(targetEntity="Language")
     */
    protected Language $language;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sport")
     */
    protected Sport $sport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Championship")
     */
    protected Championship $championship;

    /**
     * @ORM\Column(type="datetime")
     */
    protected DateTime $time;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getSport(): Sport
    {
        return $this->sport;
    }

    public function getChampionship(): Championship
    {
        return $this->championship;
    }

    public function getTime(): DateTime
    {
        return $this->time;
    }

    /**
    public function getGamesBuffered( ?string $provider = null, ?DateTime $timeFrom = null,
                                      ?DateTime $timeTo = null ): array
    {
        $result = [];
        // поскольку игры из буфера автоматически цепляются к игре, не вижу нужды еще раз лезть в БД за фильтрами
        foreach( $this->gamesBuffered as $game ) {
            if( ! empty( $provider ) && $game->getProvider()->getName() !== $provider ) {
                continue;
            }
            if( ! is_null( $timeFrom ) && $game->getTime() < $timeFrom ) {
                continue;
            }
            if( ! is_null( $timeTo ) && $timeTo < $game->getTime() ) {
                continue;
            }
            $result[] = $game;
        }
        return $result;
    }
     */

    public function setLanguage( Language $language )
    {
        $this->language = $language;
    }

    public function getConfrontation(): Confrontation
    {
        return $this->confrontation;
    }

    public function setSport( Sport $sport )
    {
        $this->sport = $sport;
    }

    public function setChampionship( Championship $championship )
    {
        $this->championship = $championship;
    }

    public function setTime( DateTime $time )
    {
        $this->time = $time;
    }

    public function setConfrontation( Confrontation $confrontation )
    {
        $this->confrontation = $confrontation;
    }
}