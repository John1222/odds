<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Сущность является связующим звеном между присланными пользователями и уже имеющимися сущностями. Например, название
 * одной и той же команды у разных провайдеров данных может отличаться. Задача данной сущности в том, чтобы при
 * получении информации по этой команде связать все эти названия с конкретным командой-сущностью, уже забитой в БД.
 * Сущность обеспечивает идентификацию следующих сущностей: чемпионаты, языки, оппоненты, провайдеры и виды спорта.
 *
 * Реализовано посредством единой таблицы, где указываются все имеющиеся связи. Рассматривалось несколько вариантов:
 * inline в виде строки, массив, единая таблица, отдельные таблицы под каждую сущность. Остановился на нормализованном
 * варианте с единой таблицей.
 *
 * Поскольку все записи живут в одной таблице, может возникнуть вопрос: зачем мешать те же языки, коих не более
 * десятка, и оппонентов, где записей будут десятки тысяч? С одной стороны логичнее раскидать на разные таблицы,
 * с другой - БД обрастает еще несколькими таблицами и начинает напоминать свалку. Больше 50 тыс записей тут вряд ли
 * наберется, инсертов мало, а под селекты можно выделить побольше кэша с индексами и нормальный сервер этой таблицы
 * не почувствует.
 *
 * Политика уникальности требует, чтобы каждой сущности соответствовало только одно уникальное имя. Например,
 * следующими записи спокойно могут сосуществовать:
 * name  language_id  opponent_id
 * ru    1
 * ru                 1
 * А вот эти уже не очень:
 * name  language_id
 * ru    1
 * ru    1
 *
 * @ORM\Entity(repositoryClass="App\Repository\IdentityLinkRepository")
 * @ORM\Table(name="`identity_links`", uniqueConstraints={
 *        @UniqueConstraint(name="idx_unq_name_and_opponent", columns={"name", "opponent_id"}),
 *        @UniqueConstraint(name="idx_unq_name_and_lang", columns={"name", "language_id"}),
 *        @UniqueConstraint(name="idx_unq_name_and_champ", columns={"name", "championship_id"}),
 *        @UniqueConstraint(name="idx_unq_name_and_sport", columns={"name", "sport_id"})
 *    }
 * )
 **/
class IdentityLink
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected int $id;

    /**
     * @ORM\Column(type="string")
     */
    protected string $name;

    /**
     * @ORM\ManyToOne(targetEntity="Opponent")
     * @ORM\JoinColumn(name="opponent_id", referencedColumnName="id", nullable=true)
     */
    protected ?Opponent $opponent = null;

    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=true)
     */
    protected ?Language $language = null;

    /**
     * @ORM\ManyToOne(targetEntity="Championship")
     * @ORM\JoinColumn(name="championship_id", referencedColumnName="id", nullable=true)
     */
    protected ?Championship $championship = null;

    /**
     * @ORM\ManyToOne(targetEntity="Sport")
     * @ORM\JoinColumn(name="sport_id", referencedColumnName="id", nullable=true)
     */
    protected ?Sport $sport = null;

    /**
     * @ORM\ManyToOne(targetEntity="Provider")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", nullable=true)
     */
    protected ?Provider $provider = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName( string $name )
    {
        $this->name = $name;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function getChampionship(): ?Championship
    {
        return $this->championship;
    }

    public function getOpponent(): ?Opponent
    {
        return $this->opponent;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setOpponent( Opponent $opponent )
    {
        $this->opponent = $opponent;
    }

    public function setLanguage( Language $language )
    {
        $this->language = $language;
    }

    public function setChampionship( Championship $championship )
    {
        $this->championship = $championship;
    }

    public function setSport( Sport $sport )
    {
        $this->sport = $sport;
    }

    public function setProvider( Provider $provider )
    {
        $this->provider = $provider;
    }
}