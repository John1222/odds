<?php
/** @noinspection PhpUnused */

namespace App\Entity;

use DateTime;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameBufferedRepository")
 * @ORM\Table(name="`games_buffer`", indexes={
 *   @ORM\Index(name="idx_games_buffer_time", columns={"time"}),
 * })
 **/
class GameBuffered
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Provider")
     */
    protected Provider $provider;
    
    /**
     * @ORM\ManyToOne(targetEntity="Confrontation")
     */
    protected Confrontation $confrontation;
    
    /**
     * @ORM\ManyToOne(targetEntity="Language")
     */
    protected Language $language;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sport")
     */
    protected Sport $sport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Championship")
     */
    protected Championship $championship;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected DateTime $time;
    
    /**
     * @ORM\ManyToOne(targetEntity="Game")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id", nullable=false)
     */
    protected ?Game $game = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getProvider(): Provider
    {
        return $this->provider;
    }

    public function setProvider( Provider $provider )
    {
        $this->provider = $provider;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage( Language $language )
    {
        $this->language = $language;
    }

    public function getSport(): Sport
    {
        return $this->sport;
    }

    public function setSport( Sport $sport )
    {
        $this->sport = $sport;
    }

    public function getChampionship(): Championship
    {
        return $this->championship;
    }

    public function setChampionship( Championship $championship )
    {
        $this->championship = $championship;
    }
    
    public function getTime(): DateTime
    {
        return $this->time;
    }

    public function getTimeAsString()
    {
        return $this->getTime()->format( 'Y-m-d H:i:s' );
    }

    /**
     * @throws Exception
     */
    public function setTime( DateTime $time )
    {
        $time->setTimezone( new DateTimeZone( 'UTC' ) );
        $this->time = $time;
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function setGame( ?Game $game )
    {
        $this->game = $game;
    }

    public function setId( int $id )
    {
        $this->id = $id;
    }

    public function getConfrontation(): Confrontation
    {
        return $this->confrontation;
    }

    public function setConfrontation( Confrontation $confrontation )
    {
        $this->confrontation = $confrontation;
    }
}