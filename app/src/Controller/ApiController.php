<?php
/** @noinspection PhpUnused */

namespace App\Controller;

use App\Entity\GameBuffered;
use App\Exception\ApiException;
use App\Service\GameMerger;
use App\Service\GameMergeRequest;
use App\Service\GameGetRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/game_get/")
     */
    public function gameGet( Request $request, GameGetRequest $gameRequest )
    {
        $gameRequest->setGame( $request->get( 'id' ) );
        $gameRequest->setProvider( $request->get( 'provider' ) );
        $gameRequest->setTimeFrom( $request->get( 'time_from' ) );
        $gameRequest->setTimeTo( $request->get( 'time_to' ) );

        /** @var GameBuffered[] $gamesBuffered */
        $gamesBuffered = $this->getDoctrine()
            ->getRepository( GameBuffered::class )
            ->findByFilter(
                $gameRequest->getGame(),
                $gameRequest->getProvider(),
                $gameRequest->getTimeFrom(),
                $gameRequest->getTimeTo()
            );

        // TODO свалить на JsonResponse
        $response = new Response;
        $response->headers->set( 'Content-Type', 'application/json' );
        $response->setContent(
            json_encode( [
                'id'           => $gameRequest->getGame()->getId(),
                'sport'        => $gameRequest->getGame()->getSport()->getName(),
                'championship' => $gameRequest->getGame()->getChampionship()->getName(),
                'opponent1'    => $gameRequest->getGame()->getConfrontation()->getOpponent1()->getName(),
                'opponent2'    => $gameRequest->getGame()->getConfrontation()->getOpponent2()->getName(),
                'game_buffer'  => ( function() use ( $gamesBuffered ) {
                    $result = [];
                    foreach( $gamesBuffered as $gameBuffered ) {
                        $result[] = [
                            'time'     => $gameBuffered->getTimeAsString(),
                            'provider' => $gameBuffered->getProvider()->getName()
                        ];
                    }
                    return $result;
                } )()
            ] )
        );
        return $response;
    }

    /**
     * Допущение: между играми одних и тех же команд в одном чемпе должна быть разница минимум двое суток. В
     * противном случае алго будет путаться
     *
     * @Route("/api/game_merge/")
     */
    public function gameMerge( Request $request, GameMerger $gameMerger )
    {
        $json = $request->getContent();
        if( empty( $json ) ) {
            throw new ApiException( 'Body of post request is empty' );
        }
        else if( trim( $json )[ 0 ] !== '[' ) {
            $json = "[{$json}]";
        }

        $games = GameMergeRequest::fromJson( $json, $this->getDoctrine()->getManager() );
        $gameMerger->merge( $games );
        
        return new Response(
            'success'
        );
    }
}