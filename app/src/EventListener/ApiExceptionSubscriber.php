<?php
namespace App\EventListener;

use App\Exception\ApiException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException( ExceptionEvent $event )
    {
        $e = $event->getThrowable();
        if( ! $e instanceof ApiException ) {
            return;
        }
        $response = new JsonResponse(
            [ 'message' => $e->getMessage() ],
            400
        );
        $response->headers->set( 'Content-Type', 'application/problem+json' );
        $event->setResponse( $response );
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => 'onKernelException'
        );
    }
}