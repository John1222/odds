<?php
/** @noinspection PhpUnused */

namespace App\Service;

use App\Entity\Championship;
use App\Entity\Language;
use App\Entity\Opponent;
use App\Entity\Provider;
use App\Entity\IdentityLink;
use App\Entity\Sport;
use App\Exception\ApiException;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class GameMergeRequest
{
    private ObjectManager $em;
    private Language $language;
    private Sport $sport;
    private Provider $provider;
    private Championship $championship;
    private Opponent $opponent1;
    private Opponent $opponent2;
    private DateTime $time;

    /**
     * @param string $json
     * @param ObjectManager $em
     * @return GameMergeRequest[]
     */
    public static function fromJson( string $json, ObjectManager $em ): array
    {
        $serializer = new Serializer( [ new ObjectNormalizer(), new ArrayDenormalizer() ], [ new JsonEncoder() ] );
        return $serializer->deserialize( $json, GameMergeRequest::class . '[]', 'json',
            [
                'default_constructor_arguments' =>
                    [
                        GameMergeRequest::class => [
                            'em' => $em
                        ]
                    ]
            ]
        );
    }
    
    public function __construct( ObjectManager $em )
    {
        $this->em = $em;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getSport(): Sport
    {
        return $this->sport;
    }

    public function getProvider(): Provider
    {
        return $this->provider;
    }

    public function getChampionship(): Championship
    {
        return $this->championship;
    }

    public function getOpponent1(): Opponent
    {
        return $this->opponent1;
    }

    public function getOpponent2(): Opponent
    {
        return $this->opponent2;
    }

    public function getTime(): DateTime
    {
        return $this->time;
    }

    public function setLanguage( string $name )
    {
        $lang = $this->em->getRepository( IdentityLink::class )->findLanguageByName( $name );
        if( is_null( $lang ) ) {
            throw new ApiException( "Do not found language \"{$name}\"" );
        }
        $this->language = $lang;
    }

    public function setSport( string $name )
    {
        $sport = $this->em->getRepository( IdentityLink::class )->findSportByName( $name );
        if( is_null( $sport ) ) {
            throw new ApiException( "Do not found sport \"{$name}\"" );
        }
        $this->sport = $sport;
    }

    public function setChampionship( string $name )
    {
        $champ = $this->em->getRepository( IdentityLink::class )->findChampionshipByName( $name );
        if( is_null( $champ ) ) {
            throw new ApiException( "Do not found championship \"{$name}\"" );
        }
        $this->championship = $champ;
    }

    public function setOpponent1( string $name )
    {
        $this->opponent1 = $this->getOpponentByName( $name );
    }

    public function setOpponent2( string $name )
    {
        $this->opponent2 = $this->getOpponentByName( $name );
    }

    /**
     * @param string $name
     * @return Opponent
     */
    private function getOpponentByName( string $name )
    {
        $opponent = $this->em->getRepository( IdentityLink::class )->findOpponentByName( $name );
        if( is_null( $opponent ) ) {
            throw new ApiException( "Do not found opponent \"{$name}\"" );
        }
        return $opponent;
    }

    public function setProvider( string $name )
    {
        $provider = $this->em->getRepository( IdentityLink::class )->findProviderByName( $name );
        if( is_null( $provider ) ) {
            throw new ApiException( "Do not found provider \"{$name}\"" );
        }
        $this->provider = $provider;
    }

    /**
     * @throws Exception
     */
    public function setTime( string $time )
    {
        try {
            $date = new DateTime( $time );
        }
        catch( Exception $e ) {
            throw new ApiException( "Time \"{$time}\" is invalid" );
        }
        $this->time = $date;
    }
}