<?php
namespace App\Service;

use App\Entity\Confrontation;
use App\Entity\Game;
use App\Entity\GameBuffered;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Exception;

class GameMerger
{
    private ObjectManager $em;

    /**
     * @param EntityManager|ObjectManager $em
     */
    public function __construct( EntityManagerInterface $em )
    {
        $this->em = $em;
    }

    /**
     * Для работы требуется анализировать таблицу и на основании этого привносить в нее новые записи. Если одновременно
     * подвисают несколько транзакций, то есть большая вероятность, что они будут работать с разными слепками БД,
     * действуя независимо друг от друга (если выключить лок, отвалится один тест). Для недопущения таких коллизий надо
     * быть уверенным, что следующая транзакция будет работать только по результатам предыдущей. Кроме как локом всей
     * таблицы я так и не придумал, как это реализовать. Я буду рад услышать альтернативные способы обойтись без этой
     * меры.
     *
     * @param GameMergeRequest[] $games
     * @throws Exception
     * @throws \Throwable
     */
    public function merge( array $games )
    {
        $this->em->transactional( function() use ( $games ) {
            $this->em->getConnection()->exec( 'LOCK TABLE games_buffer' );
            foreach( $games as $game ) {
                /** @var GameBuffered $gameBuffered */
                $gameBuffered = $this->generateGameBuffered( $game );

                if( $this->isGameBufferedExists( $gameBuffered ) ) {
                    continue;
                }

                $gameForMerge = $this->findGameForMergeWith( $gameBuffered );
                if( $gameForMerge ) {
                    $gameBuffered->setGame( $gameForMerge );
                    $this->updateGameBuffered( $gameBuffered );
                    $this->updateGameTime( $gameBuffered->getGame() );
                }
                else {
                    $gameBuffered->setGame( $this->createGame( $gameBuffered ) );
                    $this->updateGameBuffered( $gameBuffered );
                }
            }
        } );
    }

    private function updateGameBuffered( GameBuffered $gb )
    {
        $this->em->persist( $gb );
        $this->em->flush();
    }

    /**
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    private function generateGameBuffered( GameMergeRequest $game ): GameBuffered
    {
        $gameBuffered = new GameBuffered();
        $gameBuffered->setConfrontation( $this->em->getRepository( Confrontation::class )->findOrCreate(
            $game->getOpponent1(),
            $game->getOpponent2()
        ) );
        $gameBuffered->setProvider( $game->getProvider() );
        $gameBuffered->setLanguage( $game->getLanguage() );
        $gameBuffered->setSport( $game->getSport() );
        $gameBuffered->setChampionship( $game->getChampionship() );
        $gameBuffered->setTime( $game->getTime() );
        return $gameBuffered;
    }

    private function isGameBufferedExists( GameBuffered $gameBuffered ): bool
    {
        return $this->em->getRepository( GameBuffered::class )->isExists( $gameBuffered );
    }

    /**
     * В качестве игры, к которой мы будем примердживаться должна выступать игра, самая ранняя запись о которой была
     * не позднее чем сутки назад.
     *
     * Небольшое пояснение. Если плясать от самой поздней записи, то мы вместо окна размером в сутки получим
     * потенциальный шанс на растягивание этого окна до бесконечных величин: game_buffered_1 была 12 часов назад,
     * game_buffered_2 прилетела через 12 часов, за ней третья, а потом четвертая - и вот уже у нас одна игра длиною
     * в двое суток.
     */
    private function findGameForMergeWith( GameBuffered $gameToMerge ): ?Game
    {
        /** @var GameBuffered $gameWithMostEarlierTime */
        $gameWithMostEarlierTime = $this->em
            ->getRepository( GameBuffered::class )
            ->findWithMostEarlierTime( $gameToMerge );
        if( $gameWithMostEarlierTime ) {
            $differenceBetweenMostEarlierTimeAndTimeToMerge =
                $gameToMerge->getTime()->getTimestamp() - $gameWithMostEarlierTime->getTime()->getTimestamp();
            if( $differenceBetweenMostEarlierTimeAndTimeToMerge < 86400 ) {
                return $gameWithMostEarlierTime->getGame();
            }
        }
        return null;
    }

    /**
     * @throws ORMException
     */
    private function updateGameTime( Game $game )
    {
        $chosenTime = $this->em->getRepository( GameBuffered::class )->chooseTimeForGame( $game );
        $game->setTime( $chosenTime );
    }

    /**
     * @throws ORMException
     */
    private function createGame( GameBuffered $gameBuffered ): Game
    {
        $game = new Game;
        $game->setConfrontation( $gameBuffered->getConfrontation() );
        $game->setLanguage( $gameBuffered->getLanguage() );
        $game->setSport( $gameBuffered->getSport() );
        $game->setChampionship( $gameBuffered->getChampionship() );
        $game->setTime( $gameBuffered->getTime() );
        $this->em->persist( $game );
        return $game;
    }
}