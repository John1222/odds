<?php
namespace App\Service;

use App\Entity\Game;
use App\Entity\IdentityLink;
use App\Entity\Provider;
use App\Exception\ApiException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class GameGetRequest
{
    private EntityManagerInterface $em;
    private Game $game;
    private ?Provider $provider = null;
    private ?DateTime $timeFrom = null;
    private ?DateTime $timeTo = null;

    public function __construct( EntityManagerInterface $em )
    {
        $this->em = $em;
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function getTimeFrom(): ?DateTime
    {
        return $this->timeFrom;
    }

    public function getTimeTo(): ?DateTime
    {
        return $this->timeTo;
    }

    public function setGame( int $id )
    {
        $game = $this->em->getRepository( Game::class )->find( $id );
        if( is_null( $game ) ) {
            throw new ApiException( "Do not found game \"{$id}\"" );
        }
        $this->game = $game;
    }

    public function setProvider( ?string $name )
    {
        if( ! empty( $name ) ) {
            $this->provider = $this->em
                ->getRepository( IdentityLink::class )
                ->findProviderByName( $name );
            if( is_null( $this->provider ) ) {
                throw new ApiException( "Do not found provider \"{$name}\"" );
            }
        }
    }

    public function setTimeFrom( ?string $time )
    {
        if( ! empty( $time ) ) {
            try {
                $this->timeFrom = new DateTime( $time );
            }
            catch( Exception $e ) {
                throw new ApiException( "Time \"$time\" is invalid" );
            }
        }
    }

    public function setTimeTo( ?string $time )
    {
        if( ! empty( $time ) ) {
            try {
                $this->timeTo = new DateTime( $time );
            }
            catch( Exception $e ) {
                throw new ApiException( "Time \"$time\" is invalid" );
            }
        }
    }
}