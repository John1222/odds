<?php
namespace App\Repository;

use App\Entity\Game;
use App\Entity\GameBuffered;
use App\Entity\Provider;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException as NonUniqueResultExceptionAlias;
use Doctrine\ORM\NoResultException;

class GameBufferedRepository extends EntityRepository
{
    /**
     * @param Game $game
     * @return GameBuffered[]
     */
    public function findByFilter( Game $game, Provider $provider = null,
                                  DateTime $timeFrom = null, DateTime $timeTo = null ): array
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select( 'g' )
            ->from( GameBuffered::class, 'g' )
            ->where( 'g.game = :game_id' )
            ->setParameter( 'game_id', $game->getId() );
        
        if( $provider ) {
            $query->andWhere( 'g.provider = :provider' );
            $query->setParameter( 'provider', $provider->getId() );
        }
        
        if( $timeFrom && $timeTo ) {
            $query->andWhere( $query->expr()->between( 'g.time', ':from', ':to' ) );
            $query->setParameter( 'from', $timeFrom );
            $query->setParameter( 'to', $timeTo );
        }
        elseif( $timeFrom ) {
            $query->andWhere( ':from <= g.time' );
            $query->setParameter( 'from', $timeFrom );
        }
        elseif( $timeTo ) {
            $query->andWhere( 'g.time <= :to' );
            $query->setParameter( 'to', $timeTo );
        }
        
        return $query->getQuery()->getResult();
    }

    /**
     * Определяем время, которое будет записано у игры.
     *
     * Алгоритм простой:
     * 1) если в буфере всего одна запись, то ее запись и становится временем игры;
     * 2) если в буфере несколько записей, то в приоритете будет время, которое упоминается чаще всего;
     * 3) если все "времена" разные, то берем самое раннее.
     *
     * @throws NonUniqueResultExceptionAlias
     */
    public function chooseTimeForGame( Game $game ): DateTime
    {
        $query = $this->getEntityManager()->createQuery(
            '
SELECT
    g.time
FROM
    App\Entity\GameBuffered g
WHERE
    g.game = :game_id
GROUP BY
    g.time
ORDER BY
    COUNT( g.time ) DESC,
    g.time ASC'
        )
            ->setParameter( 'game_id', $game->getId() )
            ->setMaxResults( 1 );
        return $query->getOneOrNullResult()[ 'time' ];
    }

    /**
     * @throws NonUniqueResultExceptionAlias
     */
    public function isExists( GameBuffered $game ): bool
    {
        $query = $this->getEntityManager()->createQuery( '
SELECT
    g
FROM
    App\Entity\GameBuffered g
WHERE
    g.championship = :championship AND
    g.confrontation = :confrontation AND
    g.sport = :sport AND
    g.time = :time AND
    g.provider = :provider
    ' )
            ->setParameter( 'championship', $game->getChampionship() )
            ->setParameter( 'confrontation', $game->getConfrontation() )
            ->setParameter( 'sport', $game->getSport() )
            ->setParameter( 'time', $game->getTime() )
            ->setParameter( 'provider', $game->getProvider() )
            ->setMaxResults( 1 );
        return ( $query->getOneOrNullResult() instanceof GameBuffered ) ? true : false;
    }

    /**
     * Получаем самое раннее время начала из всех записей указанной игры
     *
     * @throws NonUniqueResultExceptionAlias
     */
    public function findWithMostEarlierTime( GameBuffered $game ): ?GameBuffered
    {
        $gameDuplicate = $this->findGameDuplicateInRadiusOfOneDay( $game );
        if( is_null( $gameDuplicate ) ) {
            return null;
        }

        $query = $this->getEntityManager()->createQuery(
            '
SELECT
    g,
    MIN(g.time) AS time
FROM
    App\Entity\GameBuffered g
WHERE
    g.game = :game
GROUP BY
    g
ORDER BY
    time ASC
    ' )
            ->setParameters( [
                'game' => $gameDuplicate->getId()
            ] )
            ->setMaxResults( 1 );
        $result = $query->getOneOrNullResult()[ 0 ];
        return ( is_null( $result ) ? null : $result );
    }

    /**
     * Ищем игру в радиусе +-сутки, к которой может относиться указанная
     *
     * @throws NonUniqueResultExceptionAlias
     */
    private function findGameDuplicateInRadiusOfOneDay( GameBuffered $game ): ?Game
    {
        $date = $game->getTime();
        $query = $this->getEntityManager()->createQuery(
            '
SELECT
    g
FROM
    App\Entity\GameBuffered g
WHERE
    ( g.time BETWEEN :from1 AND :to1 OR g.time BETWEEN :from2 AND :to2 ) AND
    g.confrontation = :confrontation AND
    g.championship = :championship AND
    g.sport = :sport
    ' )
            ->setParameters( [
                'from1'         => $date,
                'to1'           => ( clone $date )->modify( '+1 days' ),
                'from2'         => ( clone $date )->modify( '-1 days' ),
                'to2'           => $date,
                'confrontation' => $game->getConfrontation(),
                'championship'  => $game->getChampionship(),
                'sport'         => $game->getSport()
            ] )
            ->setMaxResults( 1 );
        try {
            return $query->getSingleResult()->getGame();
        }
        catch( NoResultException $e ) {
            return null;
        }
    }
}