<?php
namespace App\Repository;

use App\Entity\Championship;
use App\Entity\GameBuffered;
use App\Entity\Language;
use App\Entity\Opponent;
use App\Entity\Provider;
use App\Entity\IdentityLink;
use App\Entity\Sport;
use Doctrine\ORM\EntityRepository;

class IdentityLinkRepository extends EntityRepository
{
    public function findLanguageByName( string $name ): ?Language
    {
        $link = $this->findByNameFor( 'language', $name );
        return ( is_null( $link ) ) ? null : $link->getLanguage();
    }

    public function findChampionshipByName( string $name ): ?Championship
    {
        $link = $this->findByNameFor( 'championship', $name );
        return ( is_null( $link ) ) ? null : $link->getChampionship();
    }

    public function findOpponentByName( string $name ): ?Opponent
    {
        $link = $this->findByNameFor( 'opponent', $name );
        return ( is_null( $link ) ) ? null : $link->getOpponent();
    }

    public function findSportByName( string $name ): ?Sport
    {
        $link = $this->findByNameFor( 'sport', $name );
        return ( is_null( $link ) ) ? null : $link->getSport();
    }

    public function findProviderByName( string $name ): ?Provider
    {
        $link = $this->findByNameFor( 'provider', $name );
        return ( is_null( $link ) ) ? null : $link->getProvider();
    }

    private function findByNameFor( string $target, string $name ): ?IdentityLink
    {
        // докладываю: $target задается статически в пределах одного класса. Фильтровальня регуляркой мне видится
        // бесцельным откусыванием от производительности
        return $this->getEntityManager()->createQuery( "
SELECT
  p
FROM
  App\Entity\IdentityLink p
WHERE
  p.name = LOWER( :name ) AND
  p.{$target} IS NOT NULL
"
        )
            ->setParameter( 'name', $name )
            ->setMaxResults( 1 )
            ->getOneOrNullResult();
    }
}