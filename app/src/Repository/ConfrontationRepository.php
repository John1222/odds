<?php
namespace App\Repository;

use App\Entity\Confrontation;
use App\Entity\Opponent;
use Doctrine\ORM\EntityRepository;

class ConfrontationRepository extends EntityRepository
{
    public function findOrCreate( Opponent $opponent1, Opponent $opponent2 ): Confrontation
    {
        $result = $this->getEntityManager()->createQuery( '
SELECT
    c
FROM
    App\Entity\Confrontation c
WHERE
    ( c.opponent1 = :opponent1 AND c.opponent2 = :opponent2 )
    OR
    ( c.opponent1 = :opponent2 AND c.opponent2 = :opponent1 )
    ' )
            ->setParameters([
                'opponent1' => $opponent1->getId(),
                'opponent2' => $opponent2->getId()
            ])
            ->setMaxResults( 1 )
            ->getOneOrNullResult();
        if( $result ) {
            return $result;
        }
        $entity = new Confrontation;
        $entity->setOpponent1( $opponent1 );
        $entity->setOpponent2( $opponent2 );
        $this->getEntityManager()->persist( $entity );
        $this->getEntityManager()->flush();
        return $entity;
    }
}