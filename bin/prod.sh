#!/usr/bin/env bash

ROOT="$( cd "$(dirname "$0")" ; pwd -P )/../"
cd $ROOT

docker-compose build --build-arg="MODE=prod"
docker-compose up -d
docker exec -it odds_phpfpm_1 bash -c "echo 'APP_ENV=prod' > ./.env && composer install --no-dev --optimize-autoloader"