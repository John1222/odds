#!/usr/bin/env bash

ROOT="$( cd "$(dirname "$0")" ; pwd -P )/../"
cd $ROOT

docker-compose build
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
docker exec -it odds_phpfpm_1 bash -c "echo 'APP_ENV=dev' > ./.env && composer install --optimize-autoloader"